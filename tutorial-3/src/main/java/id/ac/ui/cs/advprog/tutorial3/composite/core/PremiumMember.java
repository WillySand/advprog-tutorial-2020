package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private List<Member> underlings = new ArrayList<Member>();

    public PremiumMember(String name, String role) {
        this.role = role;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if(underlings.size()<3){
            underlings.add(member);
            return;
        }
        if(role.equals("Master")){
            underlings.add(member);
            return;
        }

    }

    @Override
    public void removeChildMember(Member member) {
        underlings.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return underlings;
    }
}

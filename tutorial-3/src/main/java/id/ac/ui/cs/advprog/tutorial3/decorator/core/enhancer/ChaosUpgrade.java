package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {
    int upgradeValue;
    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 50 + rand.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return upgradeValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return "Chaos " + weapon.getDescription();
    }
}

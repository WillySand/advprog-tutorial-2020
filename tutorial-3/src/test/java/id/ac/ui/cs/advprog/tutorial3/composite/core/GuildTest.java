package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member dummy = new PremiumMember("DummyAdd", "TestAdd");
        guild.addMember(guildMaster, dummy);
        assertEquals(1, guildMaster.getChildMembers().size());
        assertEquals(2, guild.getMemberList().size());
        assertEquals(guildMaster.getChildMembers().get(0).getName(), guild.getMemberList().get(1).getName());
        assertEquals(guildMaster.getChildMembers().get(0).getRole(), guild.getMemberList().get(1).getRole());
    }

    @Test
    public void testMethodRemoveMember() {
        assertEquals(0, guildMaster.getChildMembers().size());

        Member dummy = new PremiumMember("Dummy", "Test");
        guild.addMember(guildMaster, dummy);
        assertEquals(1, guildMaster.getChildMembers().size());
        assertEquals(2, guild.getMemberList().size());

        guild.removeMember(guildMaster, dummy);
        assertEquals(1, guild.getMemberList().size());
        assertEquals(0, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member searchedMember = new OrdinaryMember("Search Me", "Target");
        guild.addMember(guildMaster, searchedMember);

        Member result = guild.getMember("Search Me", "Target");
        assertEquals(searchedMember, result);
    }
}

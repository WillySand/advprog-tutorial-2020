package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;
public class HighSpiritDefenseSpell extends HighSpiritSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return spirit.getRace() + ":Defense";
    }
    public void cast(){
        spirit.defenseStance();
    }
    public void undo(){
        super.undo();
    }
    public HighSpiritDefenseSpell(HighSpirit spirit){
        super(spirit);
    }
}

package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    Spell forUndo;
    Spell current;
    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        forUndo=current;
        current=spells.get(spellName);
        current.cast();
        // TODO: Complete Me
    }

    public void undoSpell() {
        if (forUndo==null) {
            return;
        }
        current=forUndo;
        forUndo.cast();
        forUndo=null;
        // TODO: Complete Me
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}

package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;
public class HighSpiritAttackSpell extends HighSpiritSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return spirit.getRace() + ":Attack";
    }
    public HighSpiritAttackSpell(HighSpirit spirit){
        super(spirit);
    }
    public void cast(){
        spirit.attackStance();
    }
    public void undo(){
        super.undo();
    }
}

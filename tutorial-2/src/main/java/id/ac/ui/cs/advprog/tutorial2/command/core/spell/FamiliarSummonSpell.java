package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {
	// TODO: Complete Me
    public FamiliarSummonSpell(Familiar fam){
        super(fam);
    }
    public void cast(){
        familiar.summon();
    }
    public void undo(){
        super.undo();
    }
    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }
}

package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;
    public ChainSpell(ArrayList<Spell> spells){
        this.spells=spells;
    }
    @Override
    public void cast(){
        for (Spell item: spells){
            item.cast();
        }
    }
    @Override
    public void undo(){
        for(int i=spells.size()-1;i>=0;i--){
            spells.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}

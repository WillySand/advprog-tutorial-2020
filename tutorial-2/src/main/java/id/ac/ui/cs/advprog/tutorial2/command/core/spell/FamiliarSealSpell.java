package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    // TODO: Complete Me
    public FamiliarSealSpell(Familiar fam){
        super(fam);
    }
    public void cast(){
        familiar.seal();
    }
    public void undo(){
        super.undo();
    }
    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}

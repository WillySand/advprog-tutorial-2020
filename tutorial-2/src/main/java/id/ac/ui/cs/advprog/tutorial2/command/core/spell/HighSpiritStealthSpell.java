package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;
public class HighSpiritStealthSpell extends HighSpiritSpell {
    // TODO: Complete Me
    public void cast(){
        spirit.stealthStance();
    }
    public void undo(){
        super.undo();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Stealth";
    }
    public HighSpiritStealthSpell(HighSpirit spirit){
        super(spirit);
    }
}

package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;
public class HighSpiritSealSpell extends HighSpiritSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
    public void cast(){
        spirit.seal();
    }
    public void undo(){
        super.undo();
    }
    public HighSpiritSealSpell(HighSpirit spirit){
        super(spirit);
    }
}
